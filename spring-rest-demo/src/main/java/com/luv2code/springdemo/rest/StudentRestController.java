package com.luv2code.springdemo.rest;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.luv2code.springdemo.entity.Student;

@RestController
@RequestMapping("/api")
public class StudentRestController {
	
	List<Student> students;
	
	
	@PostConstruct
	private void loadData() {
		students = new ArrayList<Student>();
		Student s1 = new Student("duda", "malo");
		Student s2 = new Student("pedro", "malo");
		Student s3 = new Student("catarina", "malo");
		this.students.add(s1);
		this.students.add(s2);
		this.students.add(s3);
	}

	@GetMapping("/students")
	public List<Student> getStudents() {
		return this.students;
	}
	
	@GetMapping("/students/{studentId}")
	public Student getStudent(@PathVariable int studentId) {
		if(studentId < 0 || studentId >= this.students.size()) {
			throw new StudentNotFoundException("Student id not found - " + studentId);
		}
		return this.students.get(studentId);
	}
	
//	@ExceptionHandler
//	public ResponseEntity<StudentErrorResponse> handleException(StudentNotFoundException exc){
//		StudentErrorResponse error = new StudentErrorResponse();
//		error.setStatus(HttpStatus.NOT_FOUND.value());
//		error.setMessage(exc.getMessage());
//		error.setTimestamp(System.currentTimeMillis());
//		return new ResponseEntity<StudentErrorResponse>(error, HttpStatus.NOT_FOUND);
//	}
//	
//	@ExceptionHandler
//	public ResponseEntity<StudentErrorResponse> handleException(Exception exc){
//		StudentErrorResponse error = new StudentErrorResponse();
//		error.setStatus(HttpStatus.BAD_REQUEST.value());
//		error.setMessage(exc.getMessage());
//		error.setTimestamp(System.currentTimeMillis());
//		return new ResponseEntity<StudentErrorResponse>(error, HttpStatus.BAD_REQUEST);
//	}
	
}
