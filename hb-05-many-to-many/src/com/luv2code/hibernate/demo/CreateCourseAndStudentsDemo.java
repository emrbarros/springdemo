package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Review;
import com.luv2code.hibernate.demo.entity.Student;

public class CreateCourseAndStudentsDemo {

	public static void main(String[] args) {
		//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class).addAnnotatedClass(Course.class).addAnnotatedClass(Review.class).addAnnotatedClass(Student.class).buildSessionFactory();
		//create session
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			
			Course tmpCourse = new Course("Pacman - How to score 1 million points");
			
			tmpCourse.addReview(new Review("great Course"));
			tmpCourse.addReview(new Review("super"));
			tmpCourse.addReview(new Review("awsome"));
			
			Student tmpStudent = new Student("Dudas", "Mal�", "dmail");
			Student tmpStudent2 = new Student("Jo�o", "Martins", "jmmail");
			Student tmpStudent3 = new Student("Catarina", "Ramos", "crmail");
			
			tmpCourse.addStudent(tmpStudent);
			tmpCourse.addStudent(tmpStudent2);
			tmpCourse.addStudent(tmpStudent3);
			
			System.out.println("saving the student");
			session.save(tmpStudent);
			session.save(tmpStudent2);
			session.save(tmpStudent3);
			System.out.println("saved the student");
			
			System.out.println("saving the course");
			session.save(tmpCourse);
			System.out.println("saved the course");
			
			session.getTransaction().commit();
			System.out.println("Done!");
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			session.close();
			factory.close();
		}
	}

}
