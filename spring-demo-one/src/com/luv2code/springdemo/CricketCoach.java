package com.luv2code.springdemo;

public class CricketCoach implements Coach {

	private FortuneService fortuneService;
	private String team;
	private String emailAdress;
	
	public CricketCoach() {
		System.out.println("Cricket Constructor");
	}
	
	@Override
	public String getDailyWorkout() {
		return "Criacket Bat";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("Cricket Setter method");
		this.fortuneService = fortuneService;
	}
	
	public String getEmailAdress() {
		return emailAdress;
	}

	public void setEmailAdress(String emailAdress) {
		System.out.println("Cricket Set email method");
		this.emailAdress = emailAdress;
	}
	
	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		System.out.println("Cricket Set Team method");
		this.team = team;
	}
}
