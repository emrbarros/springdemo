package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SetterDemoApp {

	public static void main(String[] args) {
		// load spring configuration file
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// retriebe bean from spring container
			CricketCoach theCoach = context.getBean("myCricketCoach", CricketCoach.class);
		// call methods of the bean
			System.out.println(theCoach.getDailyWorkout());
			System.out.println(theCoach.getDailyFortune());
			System.out.println(theCoach.getTeam());
			System.out.println(theCoach.getEmailAdress());
		//close the context
			context.close();
	}

}
