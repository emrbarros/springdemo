package com.luv2code.springboot.thymeleafdemo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luv2code.springboot.thymeleafdemo.dao.EmployeeRepository;
import com.luv2code.springboot.thymeleafdemo.entity.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	

	// to use with Spring Data JPA
	private EmployeeRepository employeeRepository;
	
	@Autowired
	public EmployeeServiceImpl(EmployeeRepository theEmployeeRepository) {
		super();
		this.employeeRepository = theEmployeeRepository;
	}

	@Override
	@Transactional
	public List<Employee> findAll() {
		//return this.employeeDAO.findAll();
		return this.employeeRepository.findAllByOrderByLastNameAsc();
	}

	@Override
	@Transactional
	public Employee findById(int theId) {
		//return this.employeeDAO.findById(theId);
		Optional<Employee> result = this.employeeRepository.findById(theId);
		if(result.isPresent()) {
			return result.get();
		}else {
			throw new RuntimeException("Did not find employee id - " + theId);
		}
	}

	@Override
	@Transactional
	public void save(Employee theEmployee) {
		//this.employeeDAO.save(theEmployee);
		this.employeeRepository.save(theEmployee);
	}

	@Override
	@Transactional
	public void deleteById(int theid) {
		//this.employeeDAO.deleteById(theid);
		this.employeeRepository.deleteById(theid);
	}

}
