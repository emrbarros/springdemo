<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form:form action="processForm" modelAttribute="student" >

	First name: <form:input path="firstName"/>
	
	<br><br>
	
	Last name: <form:input path="lastName"/>
	
	<br><br>
	
	<input type="submit" value="Submit" />
	
	<br><br>
	
	Country:
	
	<form:select path="country">
	
	<form:options items="${student.countryOptions}" />
	
	</form:select>
	
	<br><br>
	
	Favorite Language:
	
	Java <form:radiobutton path="favouriteLanguage" value="Java"/> 
	C <form:radiobutton path="favouriteLanguage" value="C"/>
	Scala <form:radiobutton path="favouriteLanguage" value="Scala"/>
	
	<br><br>
	
	Operating Systems:
	
	Microsoft <form:checkbox path="operatingSystems" value="Microsoft"/> 
	Linux <form:checkbox path="operatingSystems" value="Linux"/>
	Mac <form:checkbox path="operatingSystems" value="Mac"/>
	

</form:form>
</body>
</html>