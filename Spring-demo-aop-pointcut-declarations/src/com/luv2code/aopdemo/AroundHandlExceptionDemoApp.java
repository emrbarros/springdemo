package com.luv2code.aopdemo;

import java.util.logging.Logger;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import com.luv2code.aopdemo.service.TrafficFortuneService;

public class AroundHandlExceptionDemoApp {
	
	private static Logger myLogger = Logger.getLogger(AroundHandlExceptionDemoApp.class.getName());

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		TrafficFortuneService theFortuneService = context.getBean("trafficFortuneService", TrafficFortuneService.class);
		myLogger.info("\n\nMain Program: AroundDemoApp" );
		myLogger.info("Calling GetFortune");
		boolean tripwire=true;
		String result = theFortuneService.getFortune(tripwire);
		myLogger.info("\n Result is: " + result);
		myLogger.info("Finished");
		context.close();
	}
}
