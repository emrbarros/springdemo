package com.luv2code.aopdemo.aspect;

import java.util.List;
import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.luv2code.aopdemo.Account;
import com.luv2code.aopdemo.AroundWithLoggerDemoApp;

@Aspect
@Component
@Order(16)
public class MyDemoLoggingAspect {
	
	private static Logger myLogger = Logger.getLogger(AroundWithLoggerDemoApp.class.getName());
	
	@Around("execution(* com.luv2code.aopdemo.service.*.getFortune(..))")
	public Object aroundGetfortune(ProceedingJoinPoint theProceddingJoinPoint) throws Throwable {
		String method = theProceddingJoinPoint.getSignature().toShortString();
		myLogger.info("\n>>>>>>>>Executing @Around on Method: " + method);
		long begin = System.currentTimeMillis();
		Object result = null;
		try {
			result = theProceddingJoinPoint.proceed();
		} catch (Exception e) {
			myLogger.warning("\\n>>>>>>>>: " + e.getMessage());
			//result = "Major accident! but no worries your private helicopter is on the way";
			throw e;
		}
		long end = System.currentTimeMillis();
		long duration = end-begin;
		myLogger.info("\n>>>>>>>> Duration: " + duration / 1000.0 + "seconds");
		return result;
	}
	
//	@Around("execution(* com.luv2code.aopdemo.service.*.getFortune(..))")
//	public Object aroundGetfortune(ProceedingJoinPoint theProceddingJoinPoint) throws Throwable {
//		String method = theProceddingJoinPoint.getSignature().toShortString();
//		myLogger.info("\n>>>>>>>>Executing @Around on Method: " + method);
//		long begin = System.currentTimeMillis();
//		Object result = theProceddingJoinPoint.proceed();
//		long end = System.currentTimeMillis();
//		long duration = end-begin;
//		myLogger.info("\n>>>>>>>> Duration: " + duration / 1000.0 + "seconds");
//		return result;
//	}
	
	@After("execution(* com.luv2code.aopdemo.dao.AccountDAO.findAccounts(..))")
	public void afterFinallyFindAccountsAdvice(JoinPoint theJoinPoint) {
		String method = theJoinPoint.getSignature().toShortString();
		myLogger.info("\n>>>>>>>>Executing @After (finally) on Method: " + method);
	}
	
	@AfterThrowing(pointcut="execution(* com.luv2code.aopdemo.dao.AccountDAO.findAccounts(..))",throwing="exp")
	public void afterThrowingFindAccountsAdvice(JoinPoint theJoinPoint, Throwable exp) {
		String method = theJoinPoint.getSignature().toShortString();
		myLogger.info("\n>>>>>>>>Executing @AfterThrowing on Method: " + method);
		myLogger.info("\n>>>>>>>>the exception is : " + exp);
	}
	
	@AfterReturning(pointcut="execution(* com.luv2code.aopdemo.dao.AccountDAO.findAccounts(..))",returning="result")
	public void afterReturningFindAccountsAdvice(JoinPoint theJoinPoint, List<Account> result) {
		String method = theJoinPoint.getSignature().toShortString();
		
		myLogger.info("\n>>>>>>>>Executing @AfterReturning on Method: " + method);
		
		myLogger.info("\n>>>>>>>>the result is : " + result);
		
		convertAccountNamesToUpperCase(result);
		
		myLogger.info("\n>>>>>>>>the result is : " + result);
	}
	
	private void convertAccountNamesToUpperCase(List<Account> result) {
		for(Account theAccount : result) {
			theAccount.setName(theAccount.getName().toUpperCase());
		}
	}

	// this is where we add all of our related advices for logging
	//@Before("execution(public void addAccount())")// this is the Pointcut Expression Language (from AspectJ) [runs for a method called addAccount on any class]
	//("execution(public void add*())")
	//@Before("execution(public void com.luv2code.aopdemo.dao.AccountDAO.addAccount())")
	//@Before("execution(public void add*())")
	//@Before("execution(void add*())")
	//@Before("execution(* add*())")
	//@Before("execution(* add*(com.luv2code.aopdemo.Account))")
	//@Before("execution(* add*(com.luv2code.aopdemo.Account, ..))")
	//@Before("execution(* add*(..))")
	//@Before("execution(* com.luv2code.aopdemo.dao.*.*(..))")// any method inside package
	@Before("com.luv2code.aopdemo.aspect.LuvAopExpressions.forDaoPackageNoGetterNoSetter()")
	public void beforeAddAccountAdvice(JoinPoint theJoinPoint) {
		MethodSignature methodSignature = (MethodSignature) theJoinPoint.getSignature();
		myLogger.info("\n>>>>>>>> Method: " + methodSignature);
		
		Object[] args = theJoinPoint.getArgs();
		for(Object tmpArg : args) {
			myLogger.info(tmpArg.toString());
			if(tmpArg instanceof Account) {
				Account theAccount = (Account) tmpArg;
				myLogger.info(theAccount.getName());
				myLogger.info(theAccount.getLevel());
			}
		}
		
	}	
}
