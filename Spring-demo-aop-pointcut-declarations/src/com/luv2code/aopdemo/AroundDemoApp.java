package com.luv2code.aopdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import com.luv2code.aopdemo.service.TrafficFortuneService;

public class AroundDemoApp {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		TrafficFortuneService theFortuneService = context.getBean("trafficFortuneService", TrafficFortuneService.class);
		System.out.println("\n\nMain Program: AroundDemoApp" );
		System.out.println("Calling GetFortune");
		String result = theFortuneService.getFortune();
		System.out.println("\n Result is: " + result);
		System.out.println("Finished");
		context.close();
	}
}
