package com.luv2code.aopdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.luv2code.aopdemo.dao.AccountDAO;
import com.luv2code.aopdemo.dao.MembershipDAO;

public class MainDemoApp {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		AccountDAO theAccountDAO = context.getBean("accountDAO", AccountDAO.class);
		MembershipDAO theMembershipDAO = context.getBean("membershipDAO", MembershipDAO.class);
		Account theAccount = new Account();
		theAccount.setName("dudas");
		theAccount.setLevel("benfica");
		theAccountDAO.addAccount(theAccount, true);
		theAccountDAO.getName();
		theAccountDAO.getServiceCode();
		theAccountDAO.setName("aaa");
		theAccountDAO.setServiceCode("bbb");
		//theAccountDAO.test();
		//theMembershipDAO.addAccount();
		//theMembershipDAO.addSillyAccount();
		//theMembershipDAO.test();
//		System.out.println("\n lets call it again\n");
//		theAccountDAO.addAccount();
//		theMembershipDAO.addAccount();
		context.close();
	}
}
