package com.luv2code.aopdemo.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.luv2code.aopdemo.Account;

@Component
public class AccountDAO {
	
	private String name;
	private String serviceCode;
	
	public List<Account> findAccounts(boolean tripWire) {
		if(tripWire) {
			throw new RuntimeException("No soup for you!!");
		}
		System.out.println(getClass() + ": Doing my DB work");
		List<Account> myAccounts = new ArrayList<Account>();
		Account a1 =new Account("aaa", "bbb");
		Account a2 =new Account("ccc", "ddd");
		Account a3 =new Account("eee", "fff");
		myAccounts.add(a1);
		myAccounts.add(a2);
		myAccounts.add(a3);
		return myAccounts;
	}

	public void addAccount(Account theAccount, boolean vipFlag) {
		System.out.println(getClass() + ": Doing my DB work");
	}
	
	public boolean test() {
		System.out.println(getClass() + ": Doing my DB work(Test)");
		return true;
	}

	public String getName() {
		System.out.println(getClass() + ": GetName");
		return name;
	}

	public void setName(String name) {
		System.out.println(getClass() + ": SetName");
		this.name = name;
	}

	public String getServiceCode() {
		System.out.println(getClass() + ": GetServiceCode");
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		System.out.println(getClass() + ": SetServiceCode");
		this.serviceCode = serviceCode;
	}
}
