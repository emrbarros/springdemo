package com.lov2code.springdemo;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class RandomFortuneService implements FortuneService {
	
	private String[] data = {"st1", "st2", "st3"};
	
	private Random random = new Random();

	@Override
	public String getFortune() {
		return data[random.nextInt(data.length)];
	}

}
