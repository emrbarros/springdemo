package com.luv2code.springboot.cruddemo.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.luv2code.springboot.cruddemo.entity.Employee;

@Repository
public class EmployeeDAOHibernateImpl implements EmployeeDAO{
	
	private EntityManager entityManager;
	
	@Autowired
	public EmployeeDAOHibernateImpl(EntityManager theEntityManager) {
		this.entityManager = theEntityManager;
	}

	@Override
	public List<Employee> findAll() {
		Session currentSession = this.entityManager.unwrap(Session.class);
		Query<Employee> theQuery = currentSession.createQuery("from Employee", Employee.class);
		return theQuery.getResultList();
	}

	@Override
	public Employee findById(int theId) {
		Session currentSession = this.entityManager.unwrap(Session.class);
		return currentSession.get(Employee.class, theId);
	}

	@Override
	public void save(Employee theEmployee) {
		Session currentSession = this.entityManager.unwrap(Session.class);
		//theEmployee.setId(0); - to force a save
		currentSession.saveOrUpdate(theEmployee);
	}

	@Override
	public void deleteById(int theId) {
		Session currentSession = this.entityManager.unwrap(Session.class);
		//currentSession.delete(currentSession.get(Employee.class, theId));
		Query<Employee> theQuery = currentSession.createQuery("delete from Employee where id=:employeeId");
		theQuery.setParameter("employeeId", theId);
		theQuery.executeUpdate();
	}

}
