package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;

public class CreateDemo {

	public static void main(String[] args) {
		//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class).buildSessionFactory();
		//create session
		Session session = factory.getCurrentSession();
		try {
			// create a student object
			Instructor instructor = new Instructor("Chad", "Darby", "darby@luv2code.com");
			InstructorDetail instructorDetail = new InstructorDetail("www.luv2code.com", "code");
			
			instructor = new Instructor("Mhadu", "Patel", "Mhadu@luv2code.com");
			instructorDetail = new InstructorDetail("www.Madhu.com", "guitar");
			instructor.setInstructorDetail(instructorDetail);
			session.beginTransaction();
			// will save cascade
			session.save(instructor);
			session.getTransaction().commit();
			System.out.println("Done!");
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			factory.close();
		}
	}

}
