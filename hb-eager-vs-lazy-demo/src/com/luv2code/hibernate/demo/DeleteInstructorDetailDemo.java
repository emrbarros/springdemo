package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;

public class DeleteInstructorDetailDemo {

	public static void main(String[] args) {
		//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class).buildSessionFactory();
		//create session
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			int theid = 3;
			InstructorDetail instructorDetail = session.get(InstructorDetail.class, theid);
			System.out.println("Found: " + instructorDetail);
			instructorDetail.getInstructor().setInstructorDetail(null);
			session.delete(instructorDetail);
			//Instructor instructor = instructorDetail.getInstructor();
			//System.out.println("Found: " + instructor);
			session.getTransaction().commit();
			System.out.println("Done!");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
			factory.close();
		}
	}

}
