package com.luv2code.aopdemo.dao;

import org.springframework.stereotype.Component;

@Component
public class MembershipDAO {

	public void addAccount() {
		System.out.println(getClass() + ": Doing my DB work");
	}
	
	public boolean addSillyAccount() {
		System.out.println(getClass() + ": Doing my DB work(Silly)");
		return true;
	}
	
	public boolean test() {
		System.out.println(getClass() + ": Doing my DB work(Test)");
		return true;
	}
	
}
