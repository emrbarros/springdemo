package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class DeleteStudentDemo {

	public static void main(String[] args) {
		//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		//create session
		Session session = factory.getCurrentSession();
		try {
			int studentId = 2;
			//now get a new session and start a transaction
//			session = factory.getCurrentSession();
//			session.beginTransaction();
//			
//			//retrieve student based in the id
//			Student dbStudent = session.get(Student.class, studentId);
//			System.out.println("Delete: " + dbStudent);
//			session.delete(dbStudent);
//			session.getTransaction().commit();
			
			
			
			session = factory.getCurrentSession();
			session.beginTransaction();
			System.out.println("Delete: 3");
			session.createQuery("delete from Student where id=3").executeUpdate();
			
			session.getTransaction().commit();
			
			
			System.out.println("Done!");
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			factory.close();
		}
	}

}
