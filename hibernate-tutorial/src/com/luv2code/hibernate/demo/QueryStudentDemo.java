package com.luv2code.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class QueryStudentDemo {

	public static void main(String[] args) {
		//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		//create session
		Session session = factory.getCurrentSession();
		try { 
			//start a transaction
			session.beginTransaction();
			
			// query the students
			List<Student> theStudents = session.createQuery("from Student").getResultList();
			PrintStudent(theStudents);
			
			theStudents = session.createQuery("from Student s where s.lastName='benfica1'").getResultList();
			PrintStudent(theStudents);
			
			theStudents = session.createQuery("from Student s where s.lastName='benfica1' or s.firstName='daffy'").getResultList();
			PrintStudent(theStudents);
			
			theStudents = session.createQuery("from Student s where s.email LIKE 'rafa%'").getResultList();
			PrintStudent(theStudents);
			session.getTransaction().commit();
			System.out.println("Done!");
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			factory.close();
		}
	}

	private static void PrintStudent(List<Student> theStudents) {
		//display the students
		for(Student theStudent : theStudents) {
			System.out.println(theStudent);
		}
	}

}
